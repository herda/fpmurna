% Author: Maxime Herda (contact: maxime.herda@inria.fr)
% This script produces Figure 5 of "A FOKKER-PLANCK APPROACH TO THE STUDY
% OF ROBUSTNESS IN GENE EXPRESSION" by P. Degond, M. Herda and S. Mirrahimi
% arXiv 2006.14985

close all 
clear all 

Npoints = 100;
Ve = [1,8];
Vp = logspace(-1,1,Npoints);
Vg = logspace(-1,3,Npoints);



[~, ~, CVMat3D] = MomFastMuRNA2(Vg,Vp,Ve);


[P,G,E] = meshgrid(Vp,Vg,Ve);
CV0 = sqrt(1./E);
CVnorm = CVMat3D./CV0;

Fontsize = 22;
for cpt1 = 1:length(Ve)
    h = figure(cpt1);
    A = axes;
    s = surf(P(:,:,cpt1),G(:,:,cpt1),CVnorm(:,:,cpt1));
    alpha(s,.2)
    shading interp
    hold on
    [c, ~] = contour(P(:,:,cpt1),G(:,:,cpt1),CVnorm(:,:,cpt1),[2.3, 2.0,1.7,1.4,1.0,0.6,0.2]);
    clabel(c,'Interpreter','latex');
    [c, ~] = contour(P(:,:,cpt1),G(:,:,cpt1),CVnorm(:,:,cpt1),[0.4,0.8, 1.2]);
    colorbar('TickLabelInterpreter','latex')
    xlabel('$p$','Interpreter','latex')
    ylabel('$\gamma$','Interpreter','latex')
    set(gca,'fontsize', 18,'TickLabelInterpreter', 'latex')
    set(gca, 'XScale', 'log')
    set(gca, 'YScale', 'log')
    view(2)
    saveas(h,['eta',num2str(Ve(cpt1)),'.png'])
end