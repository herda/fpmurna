function [ExpMat3D, VarMat3D, CVMat3D] = MomFastMuRNA2(Vg,Vp,Ve)
%MOMFASTMURNA2 Computes the expectation, variance and CV of the mRNA
% distribution with fast "muRNA" for multiples values of the parameters 
% gamma, p and eta. This corresponds to Section 6 of the paper. Integrals 
% are computed via a Gauss-Laguerre quadrature.
% The Gauss-Laguerre abscissas and weights are computed thanks to a Matlab
% routine of Geert Van Damme:
% www.mathworks.com/matlabcentral/fileexchange/26737
%
% Author: Maxime Herda (contact: maxime.herda@inria.fr)
% For more details, see "A FOKKER-PLANCK APPROACH TO THE STUDY OF 
% ROBUSTNESS IN GENE EXPRESSION" by P. Degond, M. Herda and S. Mirrahimi
% arXiv 2006.14985
%
% INPUT: vectors of parameters
%
% Vg: Parameters $\gamma$ in the paper
% Vp: Parameters $p$ in the paper
% Ve: Parameters $\eta$ in the paper
%
% OUTPUT: 3D matrices with dimensions correponding to g, p and e
% 
% ExpMat3D: expectation of the mRNA distribution
% VarMat3D: variance of the mRNA distribution
% CVMat3D: coefficient of variation of the mRNA distribution 

% Function to be integrated against the gamma distribution
%
% \int_0^\infty integ(x) x^\alpha e^{-x} dx
integ = @(x,e,p,g,k) exp(-e*p*(log((1+x*g/e)))).*x.^k;
%
% Parameter k is the number of moment (for the distribution before change
% of variable )

ExpMat3D = zeros( length(Vg), length(Vp),length(Ve));
VarMat3D = zeros( length(Vg), length(Vp),length(Ve));
CVMat3D = zeros( length(Vg), length(Vp),length(Ve));

% The number of quadrature points is estimated for each value of delta
% taking p = 2 and g = 2. Comment between %%% to withdraw this step.
% Expected Error between quadrature with N points and N + 1 points:
err_max = 1e-7;

cpt1 = 1;
for e = Ve
    disp(['eta = ', num2str(e)]) 
    alph = e-1;% Parameter in \int_0^\infty integ(x) x^\alpha e^{-x} dx
    N = 1;
    
    %%%
    err = inf;
    CVold = inf;
    disp('Estimating number N of quadrature points...')
    while err > err_max
        [xi, wi] = GaussLaguerre(N, alph);
        g = 2;
        p = 2;
        m0 = sum(wi.*integ(xi,e,p,g,0));
        m1 = sum(wi.*integ(xi,e,p,g,1));
        m2 = sum(wi.*integ(xi,e,p,g,2));
        CV = sqrt(abs(m0*m2/(m1*m1)-1));             
        err = abs(CV - CVold);
        CVold = CV;
        N = N + 1;
    end
    %%%
    
    N = max(N,25); % taking at least 25 quadrature points
    disp(['N = ',num2str(N)])
    disp('Computing quadrature weights and abscissas...')
    [xi, wi] = GaussLaguerre(N, alph);
    disp('Computing moments...')
    cpt2 = 1;
    for p = Vp
        cpt3 = 1;
        for g = Vg
            m0 = sum(wi.*integ(xi,e,p,g,0));
            m1 = sum(wi.*integ(xi,e,p,g,1));
            m2 = sum(wi.*integ(xi,e,p,g,2));
            ExpMat3D(cpt3,cpt2,cpt1) = m1/m0;
            VarMat3D(cpt3,cpt2,cpt1) = (m2*m0-m1*m1)/(m0*m0);
            CVMat3D(cpt3,cpt2,cpt1) = sqrt(abs(m0*m2/(m1*m1)-1));
            cpt3 = cpt3 + 1;   
        end
        cpt2 = cpt2 + 1;
    end
    disp('Done.')
    disp('---------------')
    cpt1 = cpt1 + 1;
end
end

