function res = diffMcen(i,j,rvect,mvect,delt,gam,kap,nu)
% Subroutine in the assembling of the matrix
    dr = rvect(2)-rvect(1);
    dm = mvect(2)-mvect(1);
    r = rvect(i);
    m = mvect(j);
    
    if m == mvect(1)
        res = nu * (dr/dm) * (m+dm/2)^2 * ((m+dm/2)/(m)).^(-(1+r*gam)*delt*kap/-2) * exp(delt*kap*(dm/2)/(m+dm/2)/(m)/nu);
    elseif m == mvect(end)
        res = nu * (dr/dm) * (m-dm/2)^2 * ((m-dm/2)/(m)).^(-(1+r*gam)*delt*kap/nu-2) * exp(-delt*kap*(dm/2)/(m-dm/2)/(m)/nu);
    else
        res = nu * (dr/dm) * (m+dm/2)^2 * ((m+dm/2)/(m)).^(-(1+r*gam)*delt*kap/nu-2) * exp(delt*kap*(dm/2)/(m+dm/2)/(m)/nu)...
            + nu * (dr/dm) * (m-dm/2)^2 * ((m-dm/2)/(m)).^(-(1+r*gam)*delt*kap/nu-2) * exp(-delt*kap*(dm/2)/(m-dm/2)/(m)/nu);
    end
end

