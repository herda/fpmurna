function [ExpMat3D, VarMat3D, CVMat3D] = MomFastMuRNA(Vg,Vp,Vd)
%MOMFASTMURNA Computes the expectation, variance and CV of the mRNA
% distribution with fast "muRNA" for multiples values of the parameters 
% gamma, p and delta. Integrals are computed via a Gauss-Laguerre 
% quadrature after reformulation by change of variable x = d/r. 
% The Gauss-Laguerre abscissas xi and weights wi are computed thanks to a 
% routine of Geert Van Damme:
% www.mathworks.com/matlabcentral/fileexchange/26737
%
% Author: Maxime Herda (contact: maxime.herda@inria.fr)
% For more details, see "A FOKKER-PLANCK APPROACH TO THE STUDY OF 
% ROBUSTNESS IN GENE EXPRESSION" by P. Degond, M. Herda and S. Mirrahimi
% arXiv 2006.14985
%
% INPUT: Vectors of parameters
%
% Vg: Parameters $\gamma$ in the paper
% Vp: Parameters $p$ in the paper
% Vd: Parameters $\delta$ in the paper
%
% OUTPUT: 3D matrices with dimensions correponding to g, p and d
% 
% ExpMat3D: expectation of the mRNA distribution
% VarMat3D: variance of the mRNA distribution
% CVMat3D: coefficient of variation of the mRNA distribution 


% Function to be integrated against the gamma distribution
%
% \int_0^\infty integ(x) x^\alpha e^{-x} dx
integ = @(x,d,p,g,k,xmax) exp(g*d*p*(log((1+x/d/g)/(1+xmax/d/g)))).*x.^(2-k);
%
% Parameter k is the number of moment (for the distribution before change
% of variable )
%
% Term with parameter xmax is just a multiplication of the integrand by a 
% constant for numerical stability

ExpMat3D = zeros(length(Vg), length(Vp), length(Vd));
VarMat3D = zeros(length(Vg), length(Vp), length(Vd));
CVMat3D = zeros(length(Vg), length(Vp), length(Vd));


% The number of quadrature points is estimated for each value of delta
% taking p = 2 and g = 2. Comment between %%% to withdraw this step.
% Expected Error between quadrature with N points and N + 1 points:
err_max = 1e-7; 
               
cpt1 = 1;
for d = Vd
    disp(['delta = ', num2str(d)]) 
    alph = d-2; % Parameter in \int_0^\infty integ(x) x^\alpha e^{-x} dx
    
    N = 1; % Number of quadrature points
    
    %%%
    err = inf; % Error between quadrature with N points and N + 1 points
    CVold = inf;
    disp('Estimating number N of quadrature points...')
    while err > err_max
        [xi, wi] = GaussLaguerre(N, alph);
        g = 2;
        p = 2;
        m0 = sum(wi.*integ(xi,d,p,g,0,xi(end)));
        m1 = sum(wi.*integ(xi,d,p,g,1,xi(end)));
        m2 = sum(wi.*integ(xi,d,p,g,2,xi(end)));
        CV = sqrt(abs(m0*m2/(m1*m1)-1));             
        err = abs(CV - CVold);
        CVold = CV;
        N = N + 1;
    end
    %%%
    
    N = max(N,25); % taking at least 25 quadrature points
    disp(['N = ',num2str(N)])
    disp('Computing quadrature weights and abscissas...')
    [xi, wi] = GaussLaguerre(N, alph);
    disp('Computing moments...')
    cpt2 = 1;
    for p = Vp
        cpt3 = 1;
        for g = Vg
            m0 = sum(wi.*integ(xi,d,p,g,0,xi(end)));
            m1 = sum(wi.*integ(xi,d,p,g,1,xi(end)));
            m2 = sum(wi.*integ(xi,d,p,g,2,xi(end)));
            ExpMat3D(cpt3,cpt2,cpt1) = m1/m0;
            VarMat3D(cpt3,cpt2,cpt1) = (m2*m0-m1*m1)/(m0*m0);
            CVMat3D(cpt3,cpt2,cpt1) = sqrt(abs(m0*m2/(m1*m1)-1));
            cpt3 = cpt3 + 1;   
        end
        cpt2 = cpt2 + 1;
    end 
    disp('Done.')
    disp('---------------')
    cpt1 = cpt1 + 1;
end

end

