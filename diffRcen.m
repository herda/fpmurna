function res = diffRcen(i,j,rvect,mvect,delt,gam,p)
% Subroutine in the assembling of the matrix
    dr = rvect(2)-rvect(1);
    dm = mvect(2)-mvect(1);
    r = rvect(i);
    m = mvect(j);
    
    if r == rvect(1)
        res = (dm/dr) * (r+dr/2)^2 * ((r+dr/2)/(r)).^(-(1+m*p*gam)*delt-2) * exp(delt*(dr/2)/(r+dr/2)/(r));
    elseif r == rvect(end)
        res = (dm/dr) * (r-dr/2)^2 * ((r-dr/2)/(r)).^(-(1+m*p*gam)*delt-2) * exp(-delt*(dr/2)/(r-dr/2)/(r));
    else
        res = (dm/dr) * (r+dr/2)^2 * ((r+dr/2)/(r)).^(-(1+m*p*gam)*delt-2) * exp(delt*(dr/2)/(r+dr/2)/(r))...
            + (dm/dr) * (r-dr/2)^2 * ((r-dr/2)/(r)).^(-(1+m*p*gam)*delt-2) * exp(-delt*(dr/2)/(r-dr/2)/(r));
    end
end

