% Author: Maxime Herda (contact: maxime.herda@inria.fr)
% This script produces Figure 4 of "A FOKKER-PLANCK APPROACH TO THE STUDY
% OF ROBUSTNESS IN GENE EXPRESSION" by P. Degond, M. Herda and S. Mirrahimi
% arXiv 2006.14985

clear all
close all


% Vector of values for $\gamma$
Ng = 10;
Vg = linspace(0,2,Ng); 

disp(['Solving Fokker-Planck for ', num2str(Ng),' values of the parameter gamma' ])

% Other parameters:
p = 2;
delt = 8;
kap = 1;
nu = 1;

% Truncation parameters
rmax = 5;
mmax = 5;
rmin = 6e-2;
mmin = 6e-2;

% Number of points in each components
Nr = 200;
Nm = 70;
disp(['Number of degrees of freedom for each resolution = ', num2str(Nr*Nm)])

% Interface points components
rhalf = linspace(rmin,rmax,Nr+1);
mhalf = linspace(mmin,mmax,Nm+1);

% Step size in each component
dr = rhalf(2)-rhalf(1);
dm = mhalf(2)-mhalf(1);

% Cell centers components
r = rhalf(1:end-1) + dr/2.;
m = mhalf(1:end-1) + dm/2.;


cpt = 1;

disp('---------------')
for gam = Vg
    
    disp(['gamma = ',num2str(gam)])
    
    disp('Assembling the matrix...')
    M = AssembleFVMatrix(Nr, Nm, r, m, delt, gam, p, kap, nu);

    disp('Solving the Fokker-Planck equation...')
    [f,F] = SolveFVscheme(M, Nr, Nm, dr, dm);

    disp('Computing the moments of the marginal mRNA distribution...')
    rho = sum(F,1)*dm;
    m2 = sum(r.*r.*rho)*dr;
    m1 = sum(r.*rho)*dr;
    m0 = sum(rho)*dr;
    rho = rho/m0;

    disp('Computing the free mRNA distribution...')
    rho0 = r.^(-delt-2).*exp(-delt./r);
    m2_0 = sum(r.*r.*rho0)*dr;
    m1_0 = sum(r.*rho0)*dr;
    m0_0 = sum(rho0)*dr;
    rho0 = rho0/m0_0;
    
    disp('Computing the mRNA distribution with fast muRNA...')
    rhofast = exp(gam*p*delt*log((1+gam*r)*r(1)./r./(1+gam*r(1)))).*r.^(-delt-2).*exp(-delt./r);
    m2_fast = sum(r.*r.*rhofast)*dr;
    m1_fast = sum(r.*rhofast)*dr;
    m0_fast = sum(rhofast)*dr;
    rhofast = rhofast/m0_fast;

    disp('Computing the coefficients of variation for each distribution...')
    CV_0 = sqrt(abs(m2_0*m0_0/(m1_0*m1_0)-1));
    CV(cpt) = sqrt(abs(m2*m0/(m1*m1)-1))/CV_0;
    CVfast(cpt) = sqrt(abs(m2_fast*m0_fast/(m1_fast*m1_fast)-1))/CV_0;
    
    
    cpt = cpt+1;
    disp('Done.')
    disp('---------------')
end

figure
plot(Vg, CV, '-^', 'LineWidth',2, 'Markersize', 6, 'MarkerFaceColor', 'auto')
hold on
plot(Vg, CVfast, '-o', 'LineWidth',2, 'Markersize', 6, 'MarkerFaceColor', 'auto')
legend({'$CV(\rho)/CV(\rho_0)$', '$CV(\rho_{\mathrm{fast}})/CV(\rho_0)$'},'Interpreter','latex','Location', 'southwest') 
set(gca,'fontsize', 18,'TickLabelInterpreter', 'latex')
xlabel('$\gamma$','Interpreter','latex')
ylabel('Relative CV','Interpreter','latex')
xlim([0,2])
ylim([0,1])
grid on
filename = strcat('CV_p_',num2str(p),'.png');
saveas(gcf,filename)