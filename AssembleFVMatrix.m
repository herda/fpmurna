function M = AssembleFVMatrix(Nr, Nm, r, m, delt, gam, p, kap, nu)
%ASSEMBLEFVMATRIX Assembles the matrix for solving the steady Fokker-Planck
% equation
% 
% This function computes the matrix that has to be inverted for solving the
% finite volume scheme for the steady Fokker-Planck equation. The cell 
% centers (r_i, \mu_j) are ordered in lexicographical order. The last
% line of the matrix imposes the mass. The function calls subroutines for
% better readability.
%
% Author: Maxime Herda (contact: maxime.herda@inria.fr)
% For more details, see "A FOKKER-PLANCK APPROACH TO THE STUDY OF 
% ROBUSTNESS IN GENE EXPRESSION" by P. Degond, M. Herda and S. Mirrahimi
% arXiv 2006.14985
%
% INPUT:
%
% Nr: number of points in the $r$ variable
% Nm: number of points in the $\mu$ variable
% r: vector $(r_i)$ of $r$ component of the cell centers
% m: vector $(\mu_j)$ of $\mu$ component of the cell centers  
% delt: Parameter $\delta$ in the paper
% gam: Parameter $\gamma$ in the paper
% p: Parameter $p$ in the paper
% kap: Parameter $\kappa$ in the paper
% nu: Parameter $\nu$ in the paper
%
% OUTPUT: 
% 
% M: Sparse (Nr*Nm + 1) x (Nr*Nm) matrix  of the Finite Volume scheme.

M = sparse(Nr*Nm + 1, Nr*Nm);

for i = 1:Nr
    for j = 1:Nm
        %%% Defining indices
        ij = (j-1)*Nr + i;
        if i~= Nr
            ip1j = (j-1)*Nr + (i+1);
        else
            ip1j = 1;
        end
        if i~= 1
            im1j = (j-1)*Nr + (i-1);
        else
            im1j = 1;
        end
        if j~= Nm
            ijp1 = j*Nr + i;
        else
            ijp1 = 1;
        end
        if j~= 1
            ijm1 = (j-2)*Nr + i;
        else
            ijm1 = 1;
        end
        
        %%% Filling matrix
        M(ij,ij) = diffRcen(i,j,r,m,delt,gam,p)...
                 + diffMcen(i,j,r,m,delt,gam,kap,nu);
             
        %%% Right
        M(ij,ip1j) = M(ij,ip1j) + diffRright(i,j,r,m,delt,gam,p);
        %%% Left
        M(ij,im1j) = M(ij,im1j) + diffRleft(i,j,r,m,delt,gam,p);
        %%% Top
        M(ij,ijp1) = M(ij,ijp1) + diffMright(i,j,r,m,delt,gam,kap,nu);
        %%% Bottom
        M(ij,ijm1) = M(ij,ijm1) + diffMleft(i,j,r,m,delt,gam,kap,nu);
        
        %%% Last line        
        M(end, ij) = 1;
    end
end


end

