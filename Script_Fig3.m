% Author: Maxime Herda (contact: maxime.herda@inria.fr)
% This script produces Figure 3 of "A FOKKER-PLANCK APPROACH TO THE STUDY
% OF ROBUSTNESS IN GENE EXPRESSION" by P. Degond, M. Herda and S. Mirrahimi
% arXiv 2006.14985

clear all
close all


% Vector of values parameters
gam = 2;
p = 2;
delt = 8;
kap = 1;
nu = 1;
disp(['gamma = ',num2str(gam)])
disp(['p = ',num2str(p)])

% Truncation parameters
rmax = 5;
mmax = 5;
rmin = 6e-2;
mmin = 6e-2;

% Number of points in each components
Nr = 200;
Nm = 70;
disp(['Number of degrees of freedom = ', num2str(Nr*Nm)])

% Interface points components
rhalf = linspace(rmin,rmax,Nr+1);
mhalf = linspace(mmin,mmax,Nm+1);

% Step size in each component
dr = rhalf(2)-rhalf(1);
dm = mhalf(2)-mhalf(1);

% Cell centers components
r = rhalf(1:end-1) + dr/2.;
m = mhalf(1:end-1) + dm/2.;
[RR,MM] = meshgrid(r,m);

disp('Assembling the matrix...')
M = AssembleFVMatrix(Nr, Nm, r, m, delt, gam, p, kap, nu);

disp('Solving the Fokker-Planck equation...')
[f,F] = SolveFVscheme(M, Nr, Nm, dr, dm);

disp('Computing marginal densities...')
rho = sum(F,1)*dm;
m0 = sum(rho)*dr;
rho = rho/m0;

rho0 = r.^(-delt-2).*exp(-delt./r);
m0_0 = sum(rho0)*dr;
rho0 = rho0/m0_0;

rhofast = exp(gam*p*delt*log((1+gam*r)*r(1)./r./(1+gam*r(1)))).*r.^(-delt-2).*exp(-delt./r);
m0_fast = sum(rhofast)*dr;
rhofast = rhofast/m0_fast;


figure
s = surf(RR,MM,abs(F));
shading interp
axis square
view(2)
alpha(s,1)
hold on
[~,h] = contour(RR,MM,abs(F),10,'LineColor','w');
h.ContourZLevel = max(f)+1;
colorbar('TickLabelInterpreter','latex')
xlim([0,2])
ylim([0,2])
caxis([0,max(f)])
xlabel('$r$','Interpreter','latex')
ylabel('$\mu$','Interpreter','latex')
set(gca,'fontsize', 18,'TickLabelInterpreter', 'latex')
filename = strcat('surf_gam_',num2str(gam),'_p_',num2str(p),'.png');
saveas(gcf,filename)



figure
rho = sum(F,1)*dm;
m2 = sum(r.*r.*rho)*dr;
m1 = sum(r.*rho)*dr;
m0 = sum(rho)*dr;
rho = rho/m0;

rho0 = r.^(-delt-2).*exp(-delt./r);
m2_0 = sum(r.*r.*rho0)*dr;
m1_0 = sum(r.*rho0)*dr;
m0_0 = sum(rho0)*dr;
rho0 = rho0/m0_0;

rhofast = exp(gam*p*delt*log((1+gam*r)*r(1)./r./(1+gam*r(1)))).*r.^(-delt-2).*exp(-delt./r);
m2_fast = sum(r.*r.*rhofast)*dr;
m1_fast = sum(r.*rhofast)*dr;
m0_fast = sum(rhofast)*dr;
rhofast = rhofast/m0_fast;



hold on
plot(r,rho0,'-.', 'LineWidth',2, 'Markersize', 6, 'MarkerFaceColor', 'auto')
plot(r,rho,'-*', 'LineWidth',2, 'Markersize', 6, 'MarkerFaceColor', 'auto')
plot(r,rhofast,'-', 'LineWidth',2, 'Markersize', 6, 'MarkerFaceColor', 'auto')
xlabel('$r$','Interpreter','latex')
xlim([0,2])
ylim([0,max(5,max(rhofast))])
legend({'$\rho_0$','$\rho$', '$\rho_{\mathrm{fast}}$'},'Interpreter','latex') 
set(gca,'fontsize', 18,'TickLabelInterpreter', 'latex')
filename = strcat('dens_gam_',num2str(gam),'_p_',num2str(p),'.png');
saveas(gcf,filename)