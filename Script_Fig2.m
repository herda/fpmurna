% Author: Maxime Herda (contact: maxime.herda@inria.fr)
% This script produces Figure 2 of "A FOKKER-PLANCK APPROACH TO THE STUDY
% OF ROBUSTNESS IN GENE EXPRESSION" by P. Degond, M. Herda and S. Mirrahimi
% arXiv 2006.14985

close all 
clear all

% free mRNA distribution (not normalized)
rho0 = @(x,d) exp(-d./x-(2+d)*log(x));

% mRNA distribution with fast muRNA (not normalized)
rhof = @(x,d,p,g) exp(log(1+1./(g*x)).*(g*p*d)-d./x-(2+d)*log(x));

% Crude computation of the integrals of the previous distributions.We do 
% not use Gauss-Laguerre here as the precision need not be as good 
% as for Figure 1: the integrals are just used to normalize. We split the
% integral in two parts on  [0,1] and [1,infty) and change variable (x=1/r)
% in the second integral. Then a midpoint method is used for the
% resulting integral on [0,1].

integr = @(fun,x,N) (sum(fun(x)) + sum(x.^(-2).*fun(1./x)))*(x(2)-x(1));


Fontsize = 22;
%% Influence of $\gamma$

figure
hold on
N = 1000;
Nint = 10000;
vN = 1:floor(N/20):N;
x = linspace(0,2,N+1);
xint = linspace(0,1,Nint+1);
x = (x(1:N) + x(2:N+1))/2;
xint = (xint(1:Nint) + xint(2:Nint+1))/2;


fun = @(x) rho0(x,2);
yd2 = fun(x); yd2 = yd2 / integr(fun,xint,Nint);
plot(x,yd2,'k', 'LineWidth', 2)

fun = @(x) rhof(x,2,1.5,.5);
yg05 = fun(x); yg05 = yg05 / integr(fun,xint,Nint);
h = plot(x,yg05,'HandleVisibility','off', 'LineWidth', 2);
c = get(h,'Color');
plot(x(vN),yg05(vN),'o', 'Color',c, 'MarkerFaceColor', c)

fun = @(x) rhof(x,2,1.5,1);
yg1 = fun(x); yg1 = yg1 / integr(fun,xint,Nint);
h = plot(x,yg1,'HandleVisibility','off', 'LineWidth', 2);
c = get(h,'Color');
plot(x(vN),yg1(vN),'^', 'Color',c, 'MarkerFaceColor', c)

fun = @(x) rhof(x,2,1.5,2);
yg2 = fun(x); yg2 = yg2 / integr(fun,xint,Nint);
h = plot(x,yg2,'HandleVisibility','off', 'LineWidth', 2);
c = get(h,'Color');
plot(x(vN),yg2(vN),'*', 'Color',c, 'MarkerFaceColor', c)

fun = @(x) rhof(x,2,1.5,5);
yg3 = fun(x); yg3 = yg3 / integr(fun,xint,Nint);
h = plot(x,yg3,'HandleVisibility','off', 'LineWidth', 2);
c = get(h,'Color');
plot(x(vN),yg3(vN),'v', 'Color',c, 'MarkerFaceColor', c)

legend({'$\gamma = 0$ (no binding)','$\gamma = 0.5$','$\gamma = 1$','$\gamma = 2$','$\gamma = 5$'},'Interpreter','latex')

xlabel('$r$','Interpreter','latex')
ylabel('$\rho(r)$','Interpreter','latex')
set(gca,'fontsize', Fontsize,'TickLabelInterpreter', 'latex')
filename = strcat('influence_gam.png');
saveas(gcf,filename)

%% Influence of $p$

figure
hold on

fun = @(x) rho0(x,2);
yd2 = fun(x); yd2 = yd2 / integr(fun,xint,Nint);
plot(x,yd2,'k', 'LineWidth', 2)

fun = @(x) rhof(x,2,.1,1);
yp01 = fun(x); yp01 = yp01 / integr(fun,xint,Nint);
h = plot(x,yp01,'HandleVisibility','off', 'LineWidth', 2);
c = get(h,'Color');
plot(x(vN),yp01(vN),'o', 'Color',c, 'MarkerFaceColor', c)

fun = @(x) rhof(x,2,.5,1);
yp05 = fun(x); yp05 = yp05 / integr(fun,xint,Nint);
h = plot(x,yp05,'HandleVisibility','off', 'LineWidth', 2);
c = get(h,'Color');
plot(x(vN),yp05(vN),'^', 'Color',c, 'MarkerFaceColor', c)

fun = @(x) rhof(x,2,1,1);
yp1 = fun(x); yp1 = yp1 / integr(fun,xint,Nint);
h = plot(x,yp1,'HandleVisibility','off', 'LineWidth', 2);
c = get(h,'Color');
plot(x(vN),yp1(vN),'*', 'Color',c, 'MarkerFaceColor', c)

fun = @(x) rhof(x,2,3,1);
yp3 = fun(x); yp3 = yp3 / integr(fun,xint,Nint);
h = plot(x,yp3,'HandleVisibility','off', 'LineWidth', 2);
c = get(h,'Color');
plot(x(vN),yp3(vN),'v', 'Color',c, 'MarkerFaceColor', c)

legend({'$p = 0$ (no $\mu$RNA)','$p = 0.5$','$p = 1$','$p = 2$','$p = 3$'},'Interpreter','latex')

xlabel('$r$','Interpreter','latex')
ylabel('$\rho(r)$','Interpreter','latex')
set(gca,'fontsize', Fontsize,'TickLabelInterpreter', 'latex')

filename = strcat('influence_p.png');
saveas(gcf,filename)