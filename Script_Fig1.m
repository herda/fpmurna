% Author: Maxime Herda (contact: maxime.herda@inria.fr)
% This script produces Figure 1 of "A FOKKER-PLANCK APPROACH TO THE STUDY
% OF ROBUSTNESS IN GENE EXPRESSION" by P. Degond, M. Herda and S. Mirrahimi
% arXiv 2006.14985

close all 
clear all 

Npoints = 100;
Vd = [2,20];
Vp = logspace(-1,1,Npoints);
Vg = logspace(-1,3,Npoints);



[~, ~, CVMat3D] = MomFastMuRNA(Vg,Vp,Vd);


[P,G,D] = meshgrid(Vp,Vg,Vd);
CV0 = sqrt(1/(D-1));
CVnorm = CVMat3D./CV0;

Fontsize = 22;
for cpt1 = 1:length(Vd)
    h = figure(cpt1);
    A = axes;
    s = surf(P(:,:,cpt1),G(:,:,cpt1),CVnorm(:,:,cpt1));
    alpha(s,.2)
    shading interp
    hold on
    [c, ~] = contour(P(:,:,cpt1),G(:,:,cpt1),CVnorm(:,:,cpt1),[0.9,0.5,0.1]);
    clabel(c,'Interpreter','latex');
    [c, ~] = contour(P(:,:,cpt1),G(:,:,cpt1),CVnorm(:,:,cpt1),[0.95,0.8, 0.7,0.6,0.4,0.3,0.2]);
    caxis([0,1])
    colorbar('TickLabelInterpreter','latex')
    xlabel('$p$','Interpreter','latex')
    ylabel('$\gamma$','Interpreter','latex')
    set(gca,'fontsize', 18,'TickLabelInterpreter', 'latex')
    set(gca, 'XScale', 'log')
    set(gca, 'YScale', 'log')
    view(2)
    saveas(h,['delta',num2str(Vd(cpt1)),'.png'])
end


