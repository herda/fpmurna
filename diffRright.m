function res = diffRright(i,j,rvect,mvect,delt,gam,p)
% Subroutine in the assembling of the matrix
    dr = rvect(2)-rvect(1);
    dm = mvect(2)-mvect(1);
    r = rvect(i);
    m = mvect(j);
    
    if r == rvect(end)
        res = 0;
    else
        res = - (dm/dr) * (r+dr/2) ^2 * ((r+dr/2)/(r+dr)).^(-(1+m*p*gam)*delt-2) * exp(-delt*(dr/2)/(r+dr/2)/(r+dr));
    end
end

