**FPmuRNA CONTAINS THE MATLAB CODE USED FOR NUMERICAL SIMULATIONS IN:**

--------------------------------------
*A FOKKER-PLANCK APPROACH TO THE STUDY OF ROBUSTNESS IN GENE EXPRESSION*

by Pierre Degond, Maxime Herda, and Sepideh Mirrahimi

Abstract: *We study several Fokker-Planck equations arising from a stochastic chemical kinetic system modeling a gene regulatory network in biology. The densities solving the Fokker-Planck equations describe the joint distribution of the messenger RNA and micro RNA content in a cell. We provide theoretical and numerical evidences that the robustness of the gene expression is increased in the presence of micro RNA. At the mathematical level, increased robustness shows in a smaller coefficient of variation of the marginal density of the messenger RNA in the presence of micro RNA. These results follow from explicit formulas for solutions. Moreover, thanks to dimensional analyses and numerical simulations we provide qualitative insight into the role of each parameter in the model. As the increase of gene expression level comes from the underlying stochasticity in the models, we eventually discuss the choice of noise in our models and its influence on our results.*

--------------------------------------

For details, please consult the paper at https://arxiv.org/abs/2006.14985

--------------------------------------
**LIST OF THE SCRIPTS AND FUNCTIONS IN THIS ARCHIVE:**

* MomFastMuRNA.m: Function to compute the expectation, variance and cell to cell variation (coefficient of variation) of the marginal density of mRNA in the presence of fast muRNA via a Gauss-Laguerre quadrature method. See Section 4 of the paper.

* MomFastMuRNA2.m: Same as MomFastMuRNA.m for the model of Section 6 of the paper.

* GaussLaguerre.m: Function to determine the abscissas and weights of the Gauss-Laguerre quadrature. Written by Geert Van Damme [1].

* AssembleFVMatrix.m: Function to assemble the matrix corresponding to the finite volume scheme for the numerical approximation of the main Fokker-Planck model. See Section 5 of the paper.

* diffXXXX.m: Subroutines used in the matrix assembling.

* SolveFVscheme.m: Function to solve the finite volume scheme and obtain the approximate distribution function. Needs the assembled matrix.

* Script_FigX.m: The scripts used to make the numerical simulations in the paper. They can be runned as examples of use of the functions above.

**ADDITIONAL DETAILS MAY BE FOUND IN THE COMMENTS THE XXXX.m FILES**

--------------------------------------

Author: Maxime Herda (Inria Lille - Nord Europe, France)

Contact: maxime.herda@inria.fr

--------------------------------------

[1] Geert Van Damme (2020). Legendre Laguerre and Hermite - Gauss Quadrature (https://www.mathworks.com/matlabcentral/fileexchange/26737-legendre-laguerre-and-hermite-gauss-quadrature), MATLAB Central File Exchange. Retrieved September 8, 2020.