function [f,F] = SolveFVscheme(M, Nr, Nm, dr, dm)
%SOLVEFVSCHEME Solves the finite volume scheme for the staedy
% Fokker-Planck equation 
%   
% This function solves the Fokker-Planck equation. The rectangular matrix
% is inverted via the pseudo-inverse.
%
% Author: Maxime Herda (contact: maxime.herda@inria.fr)
% For more details, see "A FOKKER-PLANCK APPROACH TO THE STUDY OF 
% ROBUSTNESS IN GENE EXPRESSION" by P. Degond, M. Herda and S. Mirrahimi
% arXiv 2006.14985
%
% INPUT:
%
% M: Sparse (Nr*Nm + 1) x (Nr*Nm) matrix  of the Finite Volume scheme.
% Nr: number of points in the $r$ variable
% Nm: number of points in the $\mu$ variable
% dr: step size in the $r$ component
% dm: step size in the $\mu$ component  
%
% OUTPUT: 
% 
% f: Solution in vector form 
% F: Solution in matrix form 


% The right-hand side has only one non-zero entry which corresponds to the
% mass constraint.
b = sparse(Nm*Nr+1,1);
b(end) = 1./(dr*dm);

% For solving Mf = b we use the pseudo-inverse f = (M'*M)^{-1}*M'*b
% The matrix M'*M is inverted via a Cholesky factorization for stability
% purpose.
Mat = M'*M; 
R = chol(Mat);
f = R\(R'\(M'* b));
F = reshape(f,Nr,Nm)';

end

